﻿using Bowling.Common.Exceptions;
using Bowling.Common.Resources;
using Bowling.Domain;
using NUnit.Framework;
using System.Linq;

namespace Tests.Bowing.DomainTests
{
    public class GameContextTests
    {
        private GameContext gameContext;
        [SetUp]
        public void Setup()
        {
            gameContext = new GameContext();
            gameContext.CreateGame();
        }

        [Test]
        public void TestPerfectGame()
        {
            int[] rolls = new int[] { 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10 };
            string[] expectedFrameProgressScore = new string[] {"30","60","90","120","150","180", "210", "240", "270", "300"};
            int expectecFramesCompleted = 10;
            gameContext.AddRolls(rolls);
            gameContext.CurrentGame.UpdateScore();

            GameStatus staus = gameContext.CurrentGame.GetGameStatus();
            Assert.IsTrue(staus.GameCompleted);
            Assert.AreEqual(expectecFramesCompleted, staus.TotalFramesCompleted);

            Assert.AreEqual(expectedFrameProgressScore, staus.FrameProgressScores.ToArray());
        }

        [Test]
        public void Test10StrikesWithoutAnyBonusThrow()
        {
            int[] rolls = new int[] { 10, 10, 10, 10, 10, 10, 10, 10, 10, 10};
            string[] expectedFrameProgressScore = new string[] { "30", "60", "90", "120", "150", "180", "210", "240", "*", "*" };
            int expectecFramesCompleted = 10;
            gameContext.AddRolls(rolls);
            gameContext.CurrentGame.UpdateScore();

            GameStatus staus = gameContext.CurrentGame.GetGameStatus();
            Assert.IsFalse(staus.GameCompleted);
            Assert.AreEqual(expectecFramesCompleted, staus.TotalFramesCompleted);

            Assert.AreEqual(expectedFrameProgressScore, staus.FrameProgressScores.ToArray());
        }

        [Test]
        public void Test10StrikesFollowingFirstBonusThrow()
        {
            int[] rolls = new int[] { 10, 10, 10, 10, 10, 10, 10, 10, 10, 10,10 };
            string[] expectedFrameProgressScore = new string[] { "30", "60", "90", "120", "150", "180", "210", "240", "270", "*" };
            int expectecFramesCompleted = 10;
            gameContext.AddRolls(rolls);
            gameContext.CurrentGame.UpdateScore();

            GameStatus staus = gameContext.CurrentGame.GetGameStatus();
            Assert.IsFalse(staus.GameCompleted);
            Assert.AreEqual(expectecFramesCompleted, staus.TotalFramesCompleted);

            Assert.AreEqual(expectedFrameProgressScore, staus.FrameProgressScores.ToArray());
        }

        [Test]
        public void Test6FramesCompletedWithAllThrows1()
        {
            int[] rolls = new int[] { 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 };
            string[] expectedFrameProgressScore = new string[] { "2", "4", "6", "8", "10", "12"};
            int expectecFramesCompleted = 6;
            gameContext.AddRolls(rolls);
            gameContext.CurrentGame.UpdateScore();

            GameStatus staus = gameContext.CurrentGame.GetGameStatus();
            Assert.IsFalse(staus.GameCompleted);
            Assert.AreEqual(expectecFramesCompleted, staus.TotalFramesCompleted);

            Assert.AreEqual(expectedFrameProgressScore, staus.FrameProgressScores.ToArray());
        }

        [Test]
        public void Test7FramesCompletedWithStrikeAndSpare()
        {
            int[] rolls = new int[] { 1, 1, 1, 1, 9, 1, 2, 8, 9, 1, 10, 10 };
            string[] expectedFrameProgressScore = new string[] { "2", "4", "16", "35", "55", "*", "*" };
            int expectecFramesCompleted = 7;
            gameContext.AddRolls(rolls);
            gameContext.CurrentGame.UpdateScore();

            GameStatus staus = gameContext.CurrentGame.GetGameStatus();
            Assert.IsFalse(staus.GameCompleted);
            Assert.AreEqual(expectecFramesCompleted, staus.TotalFramesCompleted);

            Assert.AreEqual(expectedFrameProgressScore, staus.FrameProgressScores.ToArray());
        }

        [Test]
        public void TestNoRollGame()
        {
            int[] rolls = new int[] {};
            string[] expectedFrameProgressScore = new string[] {};
            int expectecFramesCompleted = 0;
            gameContext.AddRolls(rolls);
            gameContext.CurrentGame.UpdateScore();

            GameStatus staus = gameContext.CurrentGame.GetGameStatus();
            Assert.IsFalse(staus.GameCompleted);
            Assert.AreEqual(expectecFramesCompleted, staus.TotalFramesCompleted);

            Assert.AreEqual(expectedFrameProgressScore, staus.FrameProgressScores.ToArray());
        }

        [Test]
        public void TestSingleNoRollGame()
        {
            int[] rolls = new int[] {4};
            string[] expectedFrameProgressScore = new string[] {"*"};
            int expectecFramesCompleted = 1;
            gameContext.AddRolls(rolls);
            gameContext.CurrentGame.UpdateScore();

            GameStatus staus = gameContext.CurrentGame.GetGameStatus();
            Assert.IsFalse(staus.GameCompleted);
            Assert.AreEqual(expectecFramesCompleted, staus.TotalFramesCompleted);

            Assert.AreEqual(expectedFrameProgressScore, staus.FrameProgressScores.ToArray());
        }

        [Test]
        public void TestGutterBallGameFrameScorePending()
        {
            int[] rolls = new int[] { 0 };
            string[] expectedFrameProgressScore = new string[] { "*" };
            int expectecFramesCompleted = 1;
            gameContext.AddRolls(rolls);
            gameContext.CurrentGame.UpdateScore();

            GameStatus staus = gameContext.CurrentGame.GetGameStatus();
            Assert.IsFalse(staus.GameCompleted);
            Assert.AreEqual(expectecFramesCompleted, staus.TotalFramesCompleted);

            Assert.AreEqual(expectedFrameProgressScore, staus.FrameProgressScores.ToArray());
        }

        [Test]
        public void TestGutterBallGameFrameScoreAvailable()
        {
            int[] rolls = new int[] { 0, 2};
            string[] expectedFrameProgressScore = new string[] { "2" };
            int expectecFramesCompleted = 1;
            gameContext.AddRolls(rolls);
            gameContext.CurrentGame.UpdateScore();

            GameStatus staus = gameContext.CurrentGame.GetGameStatus();
            Assert.IsFalse(staus.GameCompleted);
            Assert.AreEqual(expectecFramesCompleted, staus.TotalFramesCompleted);

            Assert.AreEqual(expectedFrameProgressScore, staus.FrameProgressScores.ToArray());
        }

        [Test]
        public void TestErrorInvalidRollAttemptExceedingMoreThan10PinsDown()
        {
            int[] rolls = new int[] { 12 };
            
            Assert.Catch<InvalidRollException>(()=> gameContext.AddRolls(rolls), StringResources.EXCEPTION_INVALID_PINS_DOWN);
        }

        [Test]
        public void TestErrorInvalidRollAttemptNegativePinsDown()
        {
            int[] rolls = new int[] { -5 };

            Assert.Catch<InvalidRollException>(() => gameContext.AddRolls(rolls), StringResources.EXCEPTION_INVALID_PINS_DOWN);

            rolls = new int[] { 5, 1, 2, -8 };

            Assert.Catch<InvalidRollException>(() => gameContext.AddRolls(rolls), StringResources.EXCEPTION_INVALID_PINS_DOWN);
        }

        [Test]
        public void TestErrorInvalidRollAttemptExceedingMoreThan10PinsDownInFrame()
        {
            int[] rolls = new int[] { 2, 2, 8,7 };

            Assert.Catch<InvalidRollException>(() => gameContext.AddRolls(rolls), StringResources.EXCEPTION_INVALID_PINS_DOWN);
        }

        [Test]
        public void TestErrorFrameLimitExceededAfterAllStrikes()
        {
            int[] rolls = new int[] { 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10,10 };

            Assert.Catch<FrameLimitReachedException>(() => gameContext.AddRolls(rolls), StringResources.EXCEPTION_ERROR_MAX_FRAMES_REACHED);
        }

        [Test]
        public void TestErrorFrameLimitExceeded()
        {
            int[] rolls = new int[] { 1,2,3,4,5,4,5,1,9,1,2,3,4,5,4,4,2,2,2,2,4};

            Assert.Catch<FrameLimitReachedException>(() => gameContext.AddRolls(rolls), StringResources.EXCEPTION_ERROR_MAX_FRAMES_REACHED);
        }

        [Test]
        public void TestErrorFrameLimitExceededAfterSpare()
        {
            int[] rolls = new int[] { 10, 10, 10, 10, 10, 10, 10, 10, 10, 1, 9, 10, 10 };
            Assert.Catch<FrameLimitReachedException>(() => gameContext.AddRolls(rolls), StringResources.EXCEPTION_ERROR_MAX_FRAMES_REACHED);
        }
    }
}
