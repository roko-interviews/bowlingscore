﻿using Bowling.Common.Exceptions;
using Bowling.Common.Resources;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bowling.Domain
{
    /// <summary>
    /// A class to represent Game context.
    /// </summary>
    public class GameContext
    {
        public const int TOTAL_PINS = 10;
        public const int MAX_ROLLS_PER_FRAME = 3;
        public const int FRAMES_PER_GAME = 10;

        /// <summary>
        /// Gets the current game instance.
        /// </summary>
        public Game CurrentGame { get; private set; }
        public void CreateGame()
        {
            CurrentGame = new Game();
        }

        /// <summary>
        /// Adds rolls to curent game.
        /// </summary>
        /// <param name="rolls">Roll to add.</param>
        public void AddRolls(int[] rolls)
        {
            for(int i = 0; i < rolls.Length; i++)
            {
                switch(rolls[i])
                {
                    case int pinsDowned when pinsDowned >= 0 && pinsDowned < 10:
                        CurrentGame.AddRoll(new OrdinaryRoll(pinsDowned));

                        if (CurrentGame.RunningFrame.Rolls.Count == 1 && i + 1 < rolls.Length && rolls[i] + rolls[i + 1] == TOTAL_PINS)
                        {
                            CurrentGame.AddRoll(new Spare(rolls[i + 1]));
                            i++;
                        }
                        break;
                    case 10:
                        CurrentGame.AddRoll(new Strike());
                        break;
                    default:
                        throw new InvalidRollException(StringResources.EXCEPTION_INVALID_PINS_DOWN);
                }
            }

            CurrentGame.UpdateScore();
        }
    }
}
