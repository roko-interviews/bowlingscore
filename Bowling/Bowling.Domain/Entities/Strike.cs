﻿namespace Bowling.Domain
{
    /// <summary>
    /// Class to represent Strike.
    /// </summary>
    public class Strike : RollBase
    {
        /// <summary>
        /// Creates an instance of Strike roll.
        /// </summary>
        public Strike() : base(GameContext.TOTAL_PINS)
        {

        }
    }
}
