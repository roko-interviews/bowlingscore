﻿using System.Linq;

namespace Bowling.Domain
{
    /// <summary>
    /// A class to represent the Last Frame in a bowling game.
    /// </summary>
    public class LastFrame : Frame
    {
        /// <summary>
        /// Gets flag for whether the frame play is completed.
        /// </summary>
        public override bool HasFramePlayCompleted => _rolls.Count == _maxRollAllowed 
            || (_rolls.Count == _maxRollAllowed - 1 && _rolls.Sum(r => r.PinsDown) < 10);

        /// <summary>
        /// Creates an instance of the Last Frame in a bowling gamel.
        /// </summary>
        public LastFrame() : base()
        {
            _maxRollAllowed = 3;
        }
    }
}
