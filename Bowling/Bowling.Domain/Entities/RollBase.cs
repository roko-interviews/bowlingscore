﻿namespace Bowling.Domain
{
    /// <summary>
    /// A base class for Roll.
    /// </summary>
    public class RollBase
    {
        /// <summary>
        /// Gets the number of pins down.
        /// </summary>
        public int PinsDown { get; private set;}

        /// <summary>
        /// Creates an instance of RollBase.
        /// </summary>
        /// <param name="pinsDown">Number of pins down.</param>
        public RollBase(int pinsDown = 0)
        {
            PinsDown = pinsDown;
        }
    }
}
