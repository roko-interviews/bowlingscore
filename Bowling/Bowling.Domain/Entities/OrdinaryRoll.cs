﻿namespace Bowling.Domain
{
    /// <summary>
    /// A class to represent an Ordinary Roll.
    /// </summary>
    public class OrdinaryRoll : RollBase
    {
        /// <summary>
        /// Creates an instance of Ordinary roll
        /// </summary>
        /// <param name="pinsDown">Number of pins down.</param>
        public OrdinaryRoll(int pinsDown) : base(pinsDown)
        {

        }
    }
}
