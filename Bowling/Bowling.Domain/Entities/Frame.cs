﻿using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;

namespace Bowling.Domain
{
    /// <summary>
    /// A class to represent a frame in bowling game.
    /// </summary>
    public class Frame
    {
        protected readonly List<RollBase> _rolls;
        protected int _maxRollAllowed = 2;

        /// <summary>
        /// Gets all rolls in game.
        /// </summary>
        public IReadOnlyList<RollBase> Rolls => ImmutableList.CreateRange(_rolls);
        
        /// <summary>
        /// Gets frame progress score.
        /// </summary>
        public int? FrameProgressScore { get; private set; }
        
        /// <summary>
        /// Gets the flag for frame play completed.
        /// </summary>
        public virtual bool HasFramePlayCompleted => _rolls.Count == _maxRollAllowed 
            || (_rolls.Count == _maxRollAllowed - 1 && _rolls.Sum(r => r.PinsDown) == 10);
        
        /// <summary>
        /// Creates an instance of Frame.
        /// </summary>
        public Frame()
        {
            _rolls = new List<RollBase>();
        }

        /// <summary>
        /// Adds roll to this Frame.
        /// </summary>
        /// <param name="roll">Roll to add</param>
        public void AddRoll(RollBase roll)
        {
            if (!HasFramePlayCompleted)
            {
                _rolls.Add(roll);
            }
        }

        /// <summary>
        /// Validates an attempted roll.
        /// </summary>
        /// <param name="roll">Roll to validate.</param>
        /// <returns></returns>
        public virtual bool ValidateAttemptedRoll(RollBase roll)
        {
            return !(_rolls.Count == 1 
                && _rolls[0].PinsDown < 10 
                && _rolls[0].PinsDown + roll.PinsDown > 10);
        }

        /// <summary>
        /// Sets frame progress score.
        /// </summary>
        /// <param name="score">Score to set.</param>
        public void SetFrameProgressScore( int? score)
        {
            FrameProgressScore = score;
        }
    }
}
