﻿using System.Collections.Generic;
using System.Collections.Immutable;

namespace Bowling.Domain
{
    /// <summary>
    /// A class to represent the status of a bowling game.
    /// </summary>
    public class GameStatus
    {
        private List<string> frameProgressScores = new List<string>();
        
        /// <summary>
        /// Gets frame progress score.
        /// </summary>
        public IReadOnlyList<string> FrameProgressScores => ImmutableList.CreateRange(frameProgressScores);
        
        /// <summary>
        /// Gets the flag for game completed.
        /// </summary>
        public bool GameCompleted { get; private set; }
        
        /// <summary>
        /// Gets the number of total frames completed.
        /// </summary>
        public int TotalFramesCompleted => FrameProgressScores.Count;

        /// <summary>
        /// Adds frame score.
        /// </summary>
        /// <param name="score"></param>
        public void AddFrameScore(string score)
        {
            frameProgressScores.Add(score);
        }

        /// <summary>
        /// Clears frame score.
        /// </summary>
        public void ClearFrameScore()
        {
            frameProgressScores.Clear();
        }

        /// <summary>
        /// Sets game completed flag.
        /// </summary>
        /// <param name="isCompleted">Value for game completed flag.</param>
        public void SetGameCompleted(bool isCompleted)
        {
            GameCompleted = isCompleted;
        }

    }
}
