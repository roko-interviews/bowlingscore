﻿namespace Bowling.Domain
{
    /// <summary>
    /// A class to represent a Spare.
    /// </summary>
    public class Spare : RollBase
    {
        /// <summary>
        /// Creates an instance of Spare roll.
        /// </summary>
        /// <param name="pinsDown">Number of pins down.</param>
        public Spare(int pinsDown) : base(pinsDown)
        {

        }
    }
}
