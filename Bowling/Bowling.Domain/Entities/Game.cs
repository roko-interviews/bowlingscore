﻿using Bowling.Common.Exceptions;
using Bowling.Common.Resources;
using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bowling.Domain
{
    /// <summary>
    /// A class to represent a bowling game.
    /// </summary>
    public class Game
    {
        private readonly LinkedList<Frame> linkedFrames;

        /// <summary>
        /// Gets all frames.
        /// </summary>
        public IReadOnlyList<Frame> Frames => ImmutableList.CreateRange(linkedFrames);
        
        /// <summary>
        /// Gets the current/running node.
        /// </summary>
        public LinkedListNode<Frame> RunningNode => linkedFrames.Last;

        /// <summary>
        /// Gets the current/running frame.
        /// </summary>
        public Frame RunningFrame => RunningNode.Value;
        
        /// <summary>
        /// Gets the flag indicating whether game is completed.
        /// </summary>
        public bool Completed { get => Frames.LastOrDefault() is LastFrame && Frames.LastOrDefault().FrameProgressScore != null; }

        /// <summary>
        /// Creates an instance of bowling game.
        /// </summary>
        public Game()
        {
            linkedFrames = new LinkedList<Frame>();
        }

        /// <summary>
        /// Adds roll to this game.
        /// </summary>
        /// <param name="roll">Roll to add.</param>
        public void AddRoll(RollBase roll)
        {
            if(RunningNode == null || RunningFrame.HasFramePlayCompleted)
            {
                AddNewFrame();
            }

            if(!RunningFrame.ValidateAttemptedRoll(roll))
            {
                throw new InvalidRollException(StringResources.EXCEPTION_INVALID_PINS_DOWN);
            }

            RunningFrame.AddRoll(roll);
        }

        /// <summary>
        /// Updates score of this game.
        /// </summary>
        public void UpdateScore()
        {
            LinkedListNode<Frame> node = linkedFrames.First;

            while(node != null)
            {
                if (node.Value.HasFramePlayCompleted)
                {

                    int? previousNodeFrameScore = node.Previous == null ? 0 : node.Previous.Value.FrameProgressScore;
                    int currentNodeFrameTotalPinsDown = node.Value.Rolls.Sum(r => r.PinsDown);

                    if (node.Value is LastFrame)
                    {
                        node.Value.SetFrameProgressScore(previousNodeFrameScore + currentNodeFrameTotalPinsDown);
                    }
                    else
                    {
                        if (node.Value.Rolls.LastOrDefault() is OrdinaryRoll)
                        {
                            node.Value.SetFrameProgressScore(previousNodeFrameScore + currentNodeFrameTotalPinsDown);
                        }
                        else if (node.Value.Rolls.LastOrDefault() is Spare)
                        {
                            int? nextRollPinsDown = TotalPinsDownFromNextRoll(node);

                            if (nextRollPinsDown.HasValue)
                            {
                                node.Value.SetFrameProgressScore(previousNodeFrameScore + currentNodeFrameTotalPinsDown + nextRollPinsDown);
                            }
                        }
                        else if (node.Value.Rolls.LastOrDefault() is Strike)
                        {
                            int? next2RollsTotalPinsDown = TotalPinsDownFromNextTwoRolls(node);

                            if (next2RollsTotalPinsDown.HasValue)
                            {
                                node.Value.SetFrameProgressScore(previousNodeFrameScore + currentNodeFrameTotalPinsDown + next2RollsTotalPinsDown);
                            }
                        }
                    }
                }

                node = node.Next;
            }
        }

        /// <summary>
        /// Gets status of this game.
        /// </summary>
        /// <returns>Game status.</returns>
        public GameStatus GetGameStatus()
        {
            GameStatus status = new GameStatus();
            status.SetGameCompleted(Completed);

            foreach (Frame frame in Frames)
            {
                status.AddFrameScore(@$"{(frame.FrameProgressScore == null ? "*" : frame.FrameProgressScore)}");
            }

            return status;
        }

        private void AddNewFrame()
        {
            switch (linkedFrames.Count)
            {
                case int n when (n < 9):
                    linkedFrames.AddLast(new Frame());
                    break;
                case 9:
                    linkedFrames.AddLast(new LastFrame());
                    break;
                default:
                    throw new FrameLimitReachedException(StringResources.EXCEPTION_ERROR_MAX_FRAMES_REACHED); ;
            }
        }

        private int? TotalPinsDownFromNextRoll(LinkedListNode<Frame> node)
        {
            int? nextRollPinsDown = null;
            if (node.Next != null)
            {
                nextRollPinsDown = node.Next.Value.Rolls.FirstOrDefault().PinsDown;
            }
            return nextRollPinsDown;
        }

        private int? TotalPinsDownFromNextTwoRolls(LinkedListNode<Frame> node)
        {
            int? nextTwoRollsTotalPinsDown = null;
            if (node.Next != null)
            {
                if (node.Next.Value.Rolls.Count >= 2)
                {
                    nextTwoRollsTotalPinsDown = node.Next.Value.Rolls.Take(2).Sum(r => r.PinsDown);
                }
                else if (node.Next.Value.Rolls.Count == 1)
                {
                    nextTwoRollsTotalPinsDown = node.Next.Value.Rolls.FirstOrDefault().PinsDown;
                    if (node.Next.Next != null)
                    {
                        nextTwoRollsTotalPinsDown += node.Next.Next.Value.Rolls.FirstOrDefault().PinsDown;
                    }
                    else
                    {
                        nextTwoRollsTotalPinsDown = null;
                    }
                }
            }

            return nextTwoRollsTotalPinsDown;
        }
    }
}
