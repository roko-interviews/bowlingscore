﻿namespace Bowling.Common.Resources
{
    /// <summary>
    /// A class for all the string resources.
    /// </summary>
    public class StringResources
    {
        // Exception messages
        public const string EXCEPTION_ERROR_MAX_FRAMES_REACHED = "Maximum number of frames allowed per game has already been added.";
        public const string EXCEPTION_INVALID_PINS_DOWN = "Invalid attempt, pins down value, either exceeds available pins to take down, or is negative.";
    }
}
