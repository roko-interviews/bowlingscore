﻿using System;

namespace Bowling.Common.Exceptions
{
    /// <summary>
    /// An exception class for invalid roll.
    /// </summary>
    public class InvalidRollException : Exception
    {
        /// <summary>
        /// Creates an instance of exception.
        /// </summary>
        /// <param name="message">Exception message.</param>
        public InvalidRollException(string message) : base(message)
        {

        }
    }
}
