﻿using System;

namespace Bowling.Common.Exceptions
{
    /// <summary>
    /// An exception class for frame limit exceed situation.
    /// </summary>
    public class FrameLimitReachedException : Exception
    {
        /// <summary>
        /// Creates an instance of exception.
        /// </summary>
        /// <param name="message">Exception message</param>
        public FrameLimitReachedException(string message) : base(message)
        {
        }
    }
}
