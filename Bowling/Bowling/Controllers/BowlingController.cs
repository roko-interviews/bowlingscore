﻿using Bowling.Domain;
using Bowling.Requests;
using Bowling.Response;
using Microsoft.AspNetCore.Mvc;
using System;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Bowling.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BowlingController : ControllerBase
    {
         // POST api/<BowlingController>
        [HttpPost]
        public IActionResult Post([FromBody] PinsDownRequest request)
        {
            if (request == null)
            {
                throw new ArgumentNullException(nameof(request));
            }

            GameContext gameContext = new GameContext();
            gameContext.CreateGame();
            gameContext.AddRolls(request.PinsDowned);

            GameStatus gameStatus = gameContext.CurrentGame.GetGameStatus();

            GameStatusResponse statusResponse = new GameStatusResponse();
            statusResponse.GameCompleted = gameStatus.GameCompleted;
            statusResponse.FrameProgressScores = gameStatus.FrameProgressScores;

            return Ok(statusResponse);
        }
    }
}
