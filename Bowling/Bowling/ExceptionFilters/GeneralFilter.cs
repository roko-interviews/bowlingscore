﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using System;

namespace Bowling.ExceptionFilters
{
    public class GeneralFilter : IExceptionFilter
    {
        public void OnException(ExceptionContext context)
        {
            Exception ex = context.Exception.InnerException == null ? context.Exception : context.Exception.InnerException;
            context.Result = new ObjectResult(new Error { Message = ex.Message, ErrorCode = null }) { StatusCode = 400 };
        }
    }
}
