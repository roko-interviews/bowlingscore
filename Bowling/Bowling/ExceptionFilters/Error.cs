﻿namespace Bowling.ExceptionFilters
{
    public class Error
    {
        public string ErrorCode { get; set; }
        public string Message { get; set; }

    }
}
