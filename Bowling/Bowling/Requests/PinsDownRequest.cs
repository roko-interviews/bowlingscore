﻿using System.ComponentModel.DataAnnotations;

namespace Bowling.Requests
{
    public class PinsDownRequest
    {
        [Required]
        public int[] PinsDowned { get; set; }
    }
}
