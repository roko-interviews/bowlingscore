﻿using System.Collections.Generic;

namespace Bowling.Response
{
    public class GameStatusResponse
    {
        public IEnumerable<string> FrameProgressScores { get; set; }
        public bool GameCompleted { get; set; }
    }
}
